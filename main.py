#  https://mastodonpy.readthedocs.io/en/stable


import os
import logging

try:
    from mastodon import Mastodon
except ImportError:
    logging.error('Ne peux pas importer mastodon')

api_base_url = 'https://example.com/'
email = 'user@example.com'
password = 'password'
visibility = 'private'  # 'public', 'unlisted', 'private', or 'direct'
client_cred = '/root/.mastodon.client.secret'
user_cred = '/root/.mastodon.user.secret'

toot = 'Hello World! #helloworld'
picture = '/path/to/picture.png'

if not os.path.isfile(user_cred) or not os.path.isfile(client_cred):
    # If missing credential files
    Mastodon.create_app(
        api_base_url=api_base_url,
        to_file=client_cred
    )

    try:
        logging.info('Connexion à API Mastodon')
        mastodon = Mastodon(
            client_id=client_cred,
            api_base_url=api_base_url
        )
        mastodon.log_in(
            email,
            password,
            to_file=user_cred
        )
        mastodon = Mastodon(
            access_token=user_cred,
            api_base_url=api_base_url
        )
        mastodon.status_post(
            toot,
            media_ids=mastodon.media_post(picture),
            visibility=visibility
        )

        logging.info('à poueté: %s' % toot)
    except Exception:
        logging.exception('Erreur lors du post')
